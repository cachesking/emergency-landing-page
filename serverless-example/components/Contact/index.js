// components/Contact
import './Contact.scss';

class Contact extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      name: '',
      email: '',
      details: ''
    };
    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleInputChange(event) {
    console.log(event.target)
    const { target } = event;
    const value = target.type == 'checkbox' ? target.checked : target.value;
    const name = target.name;
    console.log(name, value)
    this.setState({ [name]: value });
  }

  handleSubmit(event) {
    alert(`Contact: ${this.state.name}: ${this.state.email}`)
    console.log('A contact was submitted: %j', this.state);
    event.preventDefault();
  }

  render() {
    return (
      <div className="Contact">
        <form onSubmit={this.handleSubmit}>
          <input
            className="Input"
            name="name"
            type="text"
            placeholder="Name"
            onChange={this.handleInputChange} />
          <input
            className="Input" 
            name="email"
            type="text"
            placeholder="Email"
            onChange={this.handleInputChange} />
            <textarea
              name="details"
              placeholder="Notes"
              onChange={this.handleInputChange}/>
          <input type="submit" value="Submit" />
        </form>
      </div>
    );
  }
}

export default Contact
