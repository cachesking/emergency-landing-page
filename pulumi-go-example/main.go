package main

import (
	"github.com/pulumi/pulumi-aws/sdk/v2/go/aws"
	"github.com/pulumi/pulumi-aws/sdk/v2/go/aws/apigateway"
	"github.com/pulumi/pulumi-aws/sdk/v2/go/aws/iam"
	"github.com/pulumi/pulumi-aws/sdk/v2/go/aws/lambda"
	"github.com/pulumi/pulumi-aws/sdk/v2/go/aws/s3"
	"github.com/pulumi/pulumi/sdk/v2/go/pulumi"
)

func main() {
	pulumi.Run(func(ctx *pulumi.Context) error {

		account, err := aws.GetCallerIdentity(ctx)
		if err != nil {
			return err
		}

		region, err := aws.GetRegion(ctx, &aws.GetRegionArgs{})
		if err != nil {
			return err
		}

		// Static site (S3 Bucket)
		f, err := NewS3Folder(ctx, "StaticSiteBucket", "./www", &FolderArgs{})
		if err != nil {
			return err
		}

		// Lambda function exection role
		role, err := iam.NewRole(ctx, "LambdaExecutionRole", &iam.RoleArgs{
			AssumeRolePolicy: pulumi.Any(map[string]interface{}{
				"Version": "2012-10-17",
				"Statement": []map[string]interface{}{
					{
						"Effect": "Allow",
						"Action": "sts:AssumeRole",
						"Principal": map[string]interface{}{
							"Service": "lambda.amazonaws.com",
						},
					},
				},
			}),
		})
		if err != nil {
			return err
		}

		logging, err := iam.NewRolePolicy(ctx, "LogsRolePolicy", &iam.RolePolicyArgs{
			Role: role.Name,
			Policy: pulumi.Any(map[string]interface{}{
				"Version": "2012-10-17",
				"Statement": []map[string]interface{}{
					{
						"Effect": "Allow",
						"Action": []interface{}{
							"logs:CreateLogGroup",
							"logs:CreateStream",
							"logs:PutLogEvents",
						},
						"Resource": "*",
					},
					{
						"Effect": "Allow",
						"Action": "s3:ListBucket",
						"Resource": []interface{}{
							pulumi.Sprintf("arn:aws:s3:::%s", bucket.Bucket),
						},
					},
					{
						"Effect": "Allow",
						"Action": []interface{}{
							"s3:PutObject",
						},
						"Resource": []interface{}{
							"arn:aws:s3:::presigned-post-data/*",
						},
					},
				},
			}),
		})
		if err != nil {
			return err
		}

		function, err := lambda.NewFunction(ctx, "ExampleLamb", &lambda.FunctionArgs{
			Code:    pulumi.NewFileArchive("./handlers/handler.zip"),
			Handler: pulumi.String("handler"),
			Runtime: pulumi.String("go1.x"),
			Role:    role.Arn,
			Environment: &lambda.FunctionEnvironmentArgs{
				Variables: pulumi.StringMap{
					"env": pulumi.String("dev"),
				},
			},
		}, pulumi.DependsOn([]pulumi.Resource{logging}))
		if err != nil {
			return err
		}

		api, err := apigateway.NewRestApi(ctx, "SomeAPI", &apigateway.RestApiArgs{
			Name:        pulumi.String("Some API"),
			Description: pulumi.String("Some API"),
			Policy: pulumi.String(`{
				"Version": "2012-10-17",
				"Statement": [{
					"Effect": "Allow",
					"Action": "sts:AssumeRole",
					"Principal": {
						"Service": "lambda.amazonaws.com"
					},
					"Sid": ""
				},{
					"Effect":    "Allow",
					"Action":    "execute-api:Invoke",
					"Resource":  "*",
					"Principal": "*",
					"Sid":       ""
				}]
			}`),
		})
		if err != nil {
			return err
		}

		things, err := apigateway.NewResource(ctx, "ThingResource", &apigateway.ResourceArgs{
			RestApi:  api.ID(),
			PathPart: pulumi.String("things"),
			ParentId: api.RootResourceId,
		}, pulumi.DependsOn([]pulumi.Resource{api}))
		if err != nil {
			return err
		}

		_, err = apigateway.NewMethod(ctx, "AnyThingMethod", &apigateway.MethodArgs{
			HttpMethod:    pulumi.String("ANY"),
			Authorization: pulumi.String("NONE"),
			RestApi:       api.ID(),
			ResourceId:    things.ID(),
		}, pulumi.DependsOn([]pulumi.Resource{api, things}))
		if err != nil {
			return err
		}

		_, err = apigateway.NewIntegration(ctx, "PostThingIntegration", &apigateway.IntegrationArgs{
			HttpMethod:            pulumi.String("ANY"),
			IntegrationHttpMethod: pulumi.String("POST"),
			ResourceId:            things.ID(),
			RestApi:               api.ID(),
			Type:                  pulumi.String("AWS_PROXY"),
			Uri:                   function.InvokeArn,
		}, pulumi.DependsOn([]pulumi.Resource{api, things, function}))
		if err != nil {
			return err
		}

		permission, err := lambda.NewPermission(ctx, "APIPermission", &lambda.PermissionArgs{
			Action:    pulumi.String("lambda:InvokeFunction"),
			Function:  function.Name,
			Principal: pulumi.String("apigateway.amazonaws.com"),
			SourceArn: pulumi.Sprintf("arn:aws:execute-api:%s:%s:%s/*/*", region.Name, account.AccountId, api.ID()),
		}, pulumi.DependsOn([]pulumi.Resource{api, things, function}))
		if err != nil {
			return err
		}

		_, err = apigateway.NewDeployment(ctx, "APIDeployment", &apigateway.DeploymentArgs{
			Description:      pulumi.String("API Deployment"),
			RestApi:          api.ID(),
			StageDescription: pulumi.String("development"),
			StageName:        pulumi.String("dev"),
		}, pulumi.DependsOn([]pulumi.Resource{api, things, function, permission}))
		if err != nil {
			return err
		}

		// Exports bucket(s) and API info
		ctx.Export("siteBucketName", f.bucketName)
		ctx.Export("websiteurl", f.websiteUrl)
		ctx.Export("API Endpoint", pulumi.Sprintf("https://%s.execute-api.%s.amazonaws.com/dev/things", api.ID(), region.Name))

		return nil
	})
}
