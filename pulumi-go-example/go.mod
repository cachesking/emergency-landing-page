module pew

go 1.14

require (
	github.com/aws/aws-lambda-go v1.17.0
	github.com/aws/aws-sdk-go v1.32.8
	github.com/pulumi/pulumi-aws/sdk/v2 v2.0.0
	github.com/pulumi/pulumi/sdk/v2 v2.0.0
)
